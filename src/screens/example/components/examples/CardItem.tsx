import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import Icon from "react-native-dynamic-vector-icons";
import RNBounceable from "@freakycoder/react-native-bounceable";
/**
 * ? Local Imports
 */
import createStyles, {
  IHomeScreenCardItemsStyle,
} from "screens/example/styles/cardItem.style";
import { ICardItem } from "@services/models";
import { P as Text } from "@shared-components/Typography";
import { IPalette } from "@theme/themes";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
  style?: CustomStyleProp;
  data: ICardItem;
  onPress: () => void;
}

const Header = ({
  styles,
  name,
  description,
}: {
  styles: IHomeScreenCardItemsStyle;
  name: string;
  description: string;
}) => (
  <>
    <Text h4 bold color={"text"}>
      {name}
    </Text>
    <Text h5 color={"placeholder"} style={styles.descriptionTextStyle}>
      {description}
    </Text>
  </>
);

const Language = ({
  styles,
  language,
}: {
  styles: IHomeScreenCardItemsStyle;
  language: string;
}) => (
  <View style={styles.languageContainer}>
    <View style={styles.languageColorStyle} />
    <Text style={styles.valueTextStyle}>{language}</Text>
  </View>
);

const Star = ({
  styles,
  colors,
  star,
}: {
  styles: IHomeScreenCardItemsStyle;
  colors: IPalette;
  star: number;
}) => (
  <View style={styles.starContainer}>
    <Icon name="star-o" type="FontAwesome" color={colors.text} />
    <Text style={styles.valueTextStyle}>{star}</Text>
  </View>
);

const Fork = ({
  styles,
  fork,
  colors,
}: {
  styles: IHomeScreenCardItemsStyle;
  fork: number;
  colors: IPalette;
}) => (
  <View style={styles.forkContainer}>
    <Icon name="code-fork" type="FontAwesome" color={colors.text} />
    <Text style={styles.valueTextStyle}>{fork}</Text>
  </View>
);

const CardItem: React.FC<ICardItemProps> = ({ style, data, onPress }) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);

  const { name, description, language, star, fork }: ICardItem = data;

  return (
    <RNBounceable style={[styles.container, style]} onPress={onPress}>
      <Header styles={styles} name={name} description={description} />
      <View style={styles.contentContainer}>
        <Language styles={styles} language={language} />
        <Star star={star} colors={colors} styles={styles} />
        <Fork styles={styles} fork={fork} colors={colors} />
      </View>
    </RNBounceable>
  );
};

export default CardItem;
