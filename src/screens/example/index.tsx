import React, { useMemo } from "react";
import { useTheme } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";
import * as NavigationService from "react-navigation-helpers";
import createStyles from "screens/example/styles";
import { SCREENS } from "@shared-constants";
import { Content, Header } from "screens/example/components/examples";

interface HomeScreenProps {}

const HomeScreen: React.FC<HomeScreenProps> = () => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);

  const handleItemPress = () => {
    NavigationService.push(SCREENS.DETAIL);
  };

  return (
    <SafeAreaView style={styles.container}>
      <Header styles={styles} colors={colors} />
      <Content
        styles={styles}
        colors={colors}
        handleItemPress={handleItemPress}
      />
    </SafeAreaView>
  );
};

export default HomeScreen;
