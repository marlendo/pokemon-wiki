import * as React from "react";
import { View, useWindowDimensions, SafeAreaView } from "react-native";
import { useTheme } from "@react-navigation/native";
import { WebView } from "react-native-webview";

import createStyles from "./styles";
import { Button } from "react-native-paper";
import ModalTokopedia from "./components";
import { IPalette } from "@theme/themes";
import { P } from "@shared-components/Typography";
import { IScrapeQueryFilyer } from "./interface";
import { mapSafe } from "utils/safe";

interface ProfileScreenProps {}

const onMessageType = {
  FILTER: "FILTER",
};

const ProfileScreen: React.FC<ProfileScreenProps> = () => {
  const webViewRef = React.useRef<any>(null);
  const theme = useTheme();
  const { width, height } = useWindowDimensions();
  const { colors }: { colors: IPalette } = theme;
  const styles = React.useMemo(() => createStyles(theme), [theme]);

  const [open, setOpen] = React.useState<boolean>(false);
  const [page, setPage] = React.useState<number>(1);
  const [filter, setFilter] = React.useState<any>([]);

  const jSInjectQueryProduct = () => {
    return `
      async function doScrape(){
        let response = await fetch("https://gql.tokopedia.com/graphql/SearchProductQueryV4", {
          "headers": {
            "accept": "*/*",
            "accept-language": "en-US,en;q=0.9,fr;q=0.8,id;q=0.7",
            "content-type": "application/json",
            "sec-ch-ua": '\"Google Chrome\";v=\"105\", \"Not)A;Brand\";v=\"8\", \"Chromium\";v=\"105\"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '\"macOS\"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "tkpd-userid": "0",
            "x-device": "desktop-0.0",
            "x-source": "tokopedia-lite",
            "x-tkpd-lite-service": "zeus",
            "x-version": "d72d26f"
          },
          "referrerPolicy": "no-referrer-when-downgrade",
          "body": JSON.stringify([{\"operationName\":\"SearchProductQueryV4\",\"variables\":{\"params\":\"device=desktop&navsource=&ob=23&page=2&q=hijab%20syari&related=true&rows=60&safe_search=false&scheme=https&shipping=&source=search&srp_component_id=02.01.00.00&srp_page_id=&srp_page_title=&st=product&start=60&topads_bucket=false&user_addressId=&user_id=&user_lat=&user_long=&user_postCode=&user_warehouseId=12210375&variants=\"},\"query\":\"query SearchProductQueryV4($params: String!) {\\n  ace_search_product_v4(params: $params) {\\n    header {\\n      totalData\\n      totalDataText\\n      processTime\\n      responseCode\\n      errorMessage\\n      additionalParams\\n      keywordProcess\\n      componentId\\n      __typename\\n    }\\n    data {\\n      banner {\\n        position\\n        text\\n        imageUrl\\n        url\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      backendFilters\\n      isQuerySafe\\n      ticker {\\n        text\\n        query\\n        typeId\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      redirection {\\n        redirectUrl\\n        departmentId\\n        __typename\\n      }\\n      related {\\n        position\\n        trackingOption\\n        relatedKeyword\\n        otherRelated {\\n          keyword\\n          url\\n          product {\\n            id\\n            name\\n            price\\n            imageUrl\\n            rating\\n            countReview\\n            url\\n            priceStr\\n            wishlist\\n            shop {\\n              city\\n              isOfficial\\n              isPowerBadge\\n              __typename\\n            }\\n            ads {\\n              adsId: id\\n              productClickUrl\\n              productWishlistUrl\\n              shopClickUrl\\n              productViewUrl\\n              __typename\\n            }\\n            badges {\\n              title\\n              imageUrl\\n              show\\n              __typename\\n            }\\n            ratingAverage\\n            labelGroups {\\n              position\\n              type\\n              title\\n              url\\n              __typename\\n            }\\n            componentId\\n            __typename\\n          }\\n          componentId\\n          __typename\\n        }\\n        __typename\\n      }\\n      suggestion {\\n        currentKeyword\\n        suggestion\\n        suggestionCount\\n        instead\\n        insteadCount\\n        query\\n        text\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      products {\\n        id\\n        name\\n        ads {\\n          adsId: id\\n          productClickUrl\\n          productWishlistUrl\\n          productViewUrl\\n          __typename\\n        }\\n        badges {\\n          title\\n          imageUrl\\n          show\\n          __typename\\n        }\\n        category: departmentId\\n        categoryBreadcrumb\\n        categoryId\\n        categoryName\\n        countReview\\n        customVideoURL\\n        discountPercentage\\n        gaKey\\n        imageUrl\\n        labelGroups {\\n          position\\n          title\\n          type\\n          url\\n          __typename\\n        }\\n        originalPrice\\n        price\\n        priceRange\\n        rating\\n        ratingAverage\\n        shop {\\n          shopId: id\\n          name\\n          url\\n          city\\n          isOfficial\\n          isPowerBadge\\n          __typename\\n        }\\n        url\\n        wishlist\\n        sourceEngine: source_engine\\n        __typename\\n      }\\n      violation {\\n        headerText\\n        descriptionText\\n        imageURL\\n        ctaURL\\n        ctaApplink\\n        buttonText\\n        buttonType\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}]),
          "method": "POST",
          "mode": "cors",
          "credentials": "include"
        });
      let data = await response.json();
      if (window.ReactNativeWebView) {
          window.ReactNativeWebView.postMessage(JSON.stringify(data));
        } else {
          console.log(data)
        }
      }
    setTimeout(function(){doScrape()},200);
    `;
  };

  const jSInjectQueryFilter = ({ query = "hijab" }: IScrapeQueryFilyer) => {
    return `
      async function doScrape(){
        let response = await fetch("https://gql.tokopedia.com/graphql/FilterSortProductQuery", {
          "headers": {
            "accept": "*/*",
            "accept-language": "en-US,en;q=0.9,fr;q=0.8,id;q=0.7",
            "content-type": "application/json",
            "sec-ch-ua": '\"Google Chrome\";v=\"105\", \"Not)A;Brand\";v=\"8\", \"Chromium\";v=\"105\"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '\"macOS\"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "tkpd-userid": "0",
            "x-device": "desktop-0.0",
            "x-source": "tokopedia-lite",
            "x-tkpd-lite-service": "zeus",
            "x-version": "d72d26f"
          },
          "referrerPolicy": "no-referrer-when-downgrade",
          "body": JSON.stringify([{\"operationName\":\"FilterSortProductQuery\",\"variables\":{\"params\":\"navsource=home&page=1&q=${query}&source=search_product&srp_component_id=02.02.02.03&st=product\"},\"query\":\"query FilterSortProductQuery($params: String!) {\\n  filter_sort_product(params: $params) {\\n    data {\\n      filter {\\n        title\\n        template_name\\n        search {\\n          searchable\\n          placeholder\\n          __typename\\n        }\\n        options {\\n          name\\n          Description\\n          key\\n          icon\\n          value\\n          inputType\\n          totalData\\n          valMax\\n          valMin\\n          hexColor\\n          child {\\n            key\\n            value\\n            name\\n            icon\\n            inputType\\n            totalData\\n            child {\\n              key\\n              value\\n              name\\n              icon\\n              inputType\\n              totalData\\n              child {\\n                key\\n                value\\n                name\\n                icon\\n                inputType\\n                totalData\\n                __typename\\n              }\\n              __typename\\n            }\\n            isPopular\\n            __typename\\n          }\\n          isPopular\\n          isNew\\n          __typename\\n        }\\n        __typename\\n      }\\n      sort {\\n        name\\n        key\\n        value\\n        inputType\\n        applyFilter\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}]),
          "method": "POST",
          "mode": "cors",
          "credentials": "include"
        });
      let data = await response.json();
      if (window.ReactNativeWebView) {
          window.ReactNativeWebView.postMessage(JSON.stringify({type: '${onMessageType.FILTER}', data}));
        } else {
          console.log(data)
        }
      }
    setTimeout(function(){doScrape()},200);
    `;
  };

  // const q = `
  // function getCookie(name) {
  //   const value = `; ${document.cookie}`;
  //   const parts = value.split(`; ${name}=`);
  //   if (parts.length === 2) return parts.pop().split(';').shift();
  // }

  // let getISID = JSON.parse(decodeURIComponent(getCookie('ISID')))['www.tokopedia.com'];
  // console.log(getISID);
  // async function doScrape(){let response=await fetch("https://gql.tokopedia.com/graphql/DynamicSearchProductQuery",{"headers":{"accept":"*/*","accept-language":"en-US,en;q=0.9,fr;q=0.8,id;q=0.7","content-type":"application/json","iris_session_id":"d3d3LnRva29wZWRpYS5jb20=.86a8c7383e358474abeaf2bfea8e8b8e.1662830100219","sec-fetch-dest":"empty","sec-fetch-mode":"cors","sec-fetch-site":"same-site","tkpd-userid":"0","x-source":"tokopedia-lite","x-tkpd-lite-service":"atreus","x-version":"49bea73"},"referrer":"https://www.tokopedia.com/search?condition=1&navsource=home&ob=5&preorder=false&q=baju%20muslim&rt=4%2C5&srp_component_id=02.01.00.00","body":"[{\"operationName\":\"DynamicSearchProductQuery\",\"variables\":{\"includeAds\":true,\"params\":\"topads_bucket=true&ob=5&device=mobile&source=search&page=1&use_page=true&related=true&q=baju%20muslim&user_id=0&safe_search=false&unique_id=985c67791f0578ad7213aa678106f421&navsource=home&condition=1&preorder=false&rt=4%2C5&srp_component_id=02.01.00.00&user_addressId=&user_cityId=176&user_districtId=2274&user_lat=&user_long=&user_postCode=&user_warehouseId=12210375\",\"adParams\":\"ob=5&ep=product&src=search&page=1&device=mobile&with_template=true&q=baju%20muslim&dep_id=&condition=1&navsource=home&preorder=false&rt=4%2C5&srp_component_id=02.01.00.00&user_addressId=&user_cityId=176&user_districtId=2274&user_lat=&user_long=&user_postCode=&user_warehouseId=12210375\"},\"query\":\"query DynamicSearchProductQuery($params: String!, $adParams: String, $includeAds: Boolean = true) {\\n  organic: ace_search_product_v4(params: $params) {\\n    header {\\n      additionalParams\\n      componentId\\n      defaultView\\n      errorMessage\\n      keywordProcess\\n      responseCode\\n      totalData\\n      totalDataText\\n      __typename\\n    }\\n    data {\\n      banner {\\n        position\\n        text\\n        imageUrl\\n        url\\n        __typename\\n      }\\n      backendFilters\\n      isQuerySafe\\n      products {\\n        id\\n        name\\n        ads {\\n          id\\n          productClickUrl\\n          productWishlistUrl\\n          productViewUrl\\n          tag\\n          __typename\\n        }\\n        categoryId\\n        categoryName\\n        childs\\n        countReview\\n        discountPercentage\\n        gaKey\\n        imageUrl\\n        customVideoURL\\n        labelGroups {\\n          position\\n          title\\n          type\\n          url\\n          __typename\\n        }\\n        labelGroupVariant {\\n          title\\n          type\\n          typeVariant: type_variant\\n          hexColor: hex_color\\n          __typename\\n        }\\n        minOrder\\n        originalPrice\\n        price\\n        rating\\n        ratingAverage\\n        shop {\\n          id\\n          name\\n          url\\n          city\\n          isOfficial\\n          isPowerBadge\\n          __typename\\n        }\\n        badges {\\n          title\\n          imageUrl\\n          show\\n          __typename\\n        }\\n        url\\n        warehouseID: warehouseIdDefault\\n        wishlist\\n        sourceEngine: source_engine\\n        __typename\\n      }\\n      redirection {\\n        redirectUrl\\n        departmentId\\n        __typename\\n      }\\n      related {\\n        position\\n        relatedKeyword\\n        trackingOption\\n        otherRelated {\\n          keyword\\n          url\\n          product {\\n            id\\n            name\\n            ads {\\n              id\\n              productClickUrl\\n              productViewUrl\\n              __typename\\n            }\\n            imageUrl\\n            rating\\n            countReview\\n            url\\n            priceStr\\n            shop {\\n              city\\n              isOfficial\\n              isPowerBadge\\n              name\\n              __typename\\n            }\\n            badges {\\n              title\\n              imageUrl\\n              show\\n              __typename\\n            }\\n            ratingAverage\\n            labelGroups {\\n              position\\n              type\\n              title\\n              url\\n              __typename\\n            }\\n            __typename\\n          }\\n          componentId\\n          __typename\\n        }\\n        __typename\\n      }\\n      suggestion {\\n        currentKeyword\\n        suggestion\\n        suggestionCount\\n        instead\\n        insteadCount\\n        text\\n        query\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      ticker {\\n        text\\n        query\\n        typeId\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      violation {\\n        headerText\\n        descriptionText\\n        imageUrl: imageURL\\n        ctaUrl: ctaURL\\n        buttonType\\n        buttonText\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n  topads: displayAdsV3(displayParams: $adParams) @include(if: $includeAds) {\\n    data {\\n      id\\n      ad_ref_key\\n      redirect\\n      sticker_id\\n      sticker_image\\n      clickTrackUrl: product_click_url\\n      shop_click_url\\n      product {\\n        id\\n        name\\n        wishlist\\n        image {\\n          s_ecs\\n          s_url\\n          __typename\\n        }\\n        url: uri\\n        relative_uri\\n        price: price_format\\n        campaign {\\n          original_price\\n          discount_percentage\\n          __typename\\n        }\\n        wholeSalePrice: wholesale_price {\\n          quantityMin: quantity_min_format\\n          quantityMax: quantity_max_format\\n          price: price_format\\n          __typename\\n        }\\n        countReview: count_review_format\\n        category {\\n          id\\n          __typename\\n        }\\n        minOrder: product_minimum_order\\n        product_wholesale\\n        free_return\\n        isNewProduct: product_new_label\\n        cashback: product_cashback_rate\\n        rating: product_rating\\n        ratingAverage: product_rating_format\\n        top_label\\n        labelGroups: label_group {\\n          position\\n          type\\n          title\\n          url\\n          __typename\\n        }\\n        categoryBreadcrumb: category_breadcrumb\\n        __typename\\n      }\\n      shop {\\n        id\\n        name\\n        city\\n        isPowerBadge: gold_shop\\n        gold_shop_badge\\n        isOfficial: shop_is_official\\n        url: uri\\n        owner_id\\n        is_owner\\n        badges {\\n          title\\n          imageUrl: image_url\\n          show\\n          __typename\\n        }\\n        __typename\\n      }\\n      applinks\\n      tag\\n      __typename\\n    }\\n    template {\\n      isAd: is_ad\\n      __typename\\n    }\\n    __typename\\n  }\\n  inspirationCarousel: searchInspirationCarouselV2(params: $params) {\\n    data {\\n      title\\n      position\\n      type\\n      layout\\n      options {\\n        component_id\\n        title\\n        url\\n        product {\\n          id\\n          name\\n          price\\n          imageUrl: image_url\\n          rating\\n          countReview: count_review\\n          url\\n          applink\\n          priceStr: price_str\\n          description\\n          shop {\\n            name\\n            city\\n            __typename\\n          }\\n          badges {\\n            title\\n            imageUrl: image_url\\n            show\\n            __typename\\n          }\\n          wishlist\\n          freeOngkir {\\n            isActive\\n            imageUrl: image_url\\n            __typename\\n          }\\n          ratingAverage: rating_average\\n          labelGroups: label_groups {\\n            position\\n            title\\n            type\\n            url\\n            __typename\\n          }\\n          ads {\\n            id\\n            productClickUrl\\n            productWishlistUrl\\n            shopClickUrl\\n            productViewUrl\\n            __typename\\n          }\\n          __typename\\n        }\\n        __typename\\n      }\\n      tracking_option\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}]","method":"POST","mode":"cors","credentials":"include"});
  // let data = await response.json();
  // if (window.ReactNativeWebView) {
  //  window.ReactNativeWebView.postMessage(JSON.stringify(data));
  // } else {
  //     console.log(data)
  // }
  // }
  // setTimeout(function(){doScrape()},200)
  // `;

  const doScrape = () => {
    if (webViewRef) {
      webViewRef.current.injectJavaScript(jSInjectQueryProduct());
    }
  };

  const doScrapeFilter = (e: IScrapeQueryFilyer) => {
    if (webViewRef) {
      webViewRef.current.injectJavaScript(jSInjectQueryFilter(e));
    }
  };

  const doneScrapeFilter = (payload: any) => {
    let listFilter = mapSafe(
      () => payload[0].data.filter_sort_product.data.filter,
      false,
    );
    if (listFilter) {
      let categoryFilter = mapSafe(() => {
        let dataFilter = listFilter.filter((row: any) => {
          return row.template_name === "template_category";
        });
        return dataFilter[0].options;
      }, false);
      let data = mapSafe(() => {
        let newData: any[] = [];
        for (let row of categoryFilter) {
          if (row.key === "sc") {
            let newChild: any[] = [];
            if (row.child.length > 0) {
              let xNewChild: any[] = [];
              for (let xRow of row.child) {
                if (xRow.key === "sc") {
                  let yNewChild: any[] = [];
                  if (xRow.child.length > 0) {
                    for (let yRow of xRow.child) {
                      if (yRow.key === "sc") {
                        yNewChild.push({
                          icon: yRow.icon,
                          name: yRow.name,
                          value: yRow.value,
                          child: [],
                        });
                      }
                    }
                  }
                  xNewChild.push({
                    icon: xRow.icon,
                    name: xRow.name,
                    value: xRow.value,
                    child: yNewChild,
                  });
                }
              }
              newChild.push(xNewChild);
            }
            newData.push({
              icon: row.icon,
              name: row.name,
              value: row.value,
              child: newChild,
            });
          }
        }
        return newData;
      }, []);
      setFilter(data);
    } else {
      setFilter([]);
    }
  };

  const onMessage = (e: any) => {
    let newMessage: { type: string; data: string } = mapSafe(
      () => {
        let parseData = JSON.parse(e.nativeEvent.data);
        return { type: parseData.type, data: parseData.data };
      },
      { type: null, data: null },
    );

    let { type, data } = newMessage;
    switch (type) {
      case onMessageType.FILTER:
        doneScrapeFilter(data);
        break;
      default:
        console.log("ON_MESSAGE_UNHANDLED", newMessage);
        break;
    }
  };

  const handleSearch = (e: string) => {
    let query = encodeURI(e);
    console.log("ON_SEARCH", query);
    if (page === 1) {
      doScrapeFilter({ query });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ModalTokopedia
        open={open}
        setOpen={setOpen}
        handleSearch={handleSearch}
        filterData={filter}
      />
      <WebView
        ref={webViewRef}
        userAgent={
          "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"
        }
        source={{ uri: "https://www.tokopedia.com/" }}
        style={{ width, height }}
        onMessage={onMessage}
      />
      <View>
        <Button
          style={{
            width,
            borderRadius: 0,
          }}
          color={colors.tokopedia}
          mode={"contained"}
          onPress={() => {
            setOpen(true);
          }}
        >
          <P fontStyle={"bold"} color={"white"}>
            Open Tokopedia Scraper
          </P>
        </Button>
      </View>
    </SafeAreaView>
  );
};

export default ProfileScreen;
