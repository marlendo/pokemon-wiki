import * as React from "react";
import { useTheme } from "@react-navigation/native";
import { Appbar, Searchbar } from "react-native-paper";
import { IPalette } from "@theme/themes";
import { montserrat } from "@fonts";
import useDebounce from "utils/useDebounce";

interface HeaderModalTokopediaProps {
  open: boolean;
  setOpen: (e: boolean) => void;
  searchMode: boolean;
  setSearchMode: (e: boolean) => void;
  onChanged: (e: string) => void;
}

const HeaderModalTokopedia: React.FC<HeaderModalTokopediaProps> = ({
  open,
  setOpen,
  searchMode,
  setSearchMode,
  onChanged,
}) => {
  const theme = useTheme();
  const { colors }: { colors: IPalette } = theme;
  const [search, setSearch] = React.useState<string>("");
  const searchChanged = useDebounce(search, 1000);

  React.useEffect(() => {
    if (open && searchChanged.length > 0) {
      onChanged(searchChanged ? searchChanged.toLowerCase() : "");
    }
  }, [open, searchChanged]);

  return (
    <Appbar.Header
      style={{
        backgroundColor: colors.tokopedia,
      }}
    >
      {searchMode ? (
        <Searchbar
          placeholder="Search Product"
          onChangeText={(e: string) => {
            setSearch(e);
            if (e.length === 0) {
              setSearchMode(false);
            }
          }}
          autoFocus
          value={search}
          inputStyle={{
            fontFamily: montserrat["regular"],
            fontSize: 12,
          }}
        />
      ) : (
        <>
          <Appbar.BackAction
            onPress={() => {
              setOpen(false);
            }}
          />
          <Appbar.Content title="Scrape Tokopedia" />
          <Appbar.Action
            icon="magnify"
            onPress={() => {
              setSearchMode(true);
            }}
          />
        </>
      )}
    </Appbar.Header>
  );
};

export default HeaderModalTokopedia;
