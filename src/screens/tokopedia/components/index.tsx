import * as React from "react";
import { useTheme } from "@react-navigation/native";
import { Image, Modal, useWindowDimensions, View } from "react-native";
import { Button } from "react-native-paper";
import { IPalette } from "@theme/themes";
import createStyles from "../styles";
import { P } from "@shared-components/Typography";
import HeaderModalTokopedia from "./header";
import { IFilterTokopedia } from "../interface";
import FilterTokopedia from "./filter";

interface ModalTokopediaProps {
  open: boolean;
  setOpen: (e: boolean) => void;
  handleSearch: (e: string) => void;
  filterData: IFilterTokopedia;
}

const ModalTokopedia: React.FC<ModalTokopediaProps> = ({
  open,
  setOpen,
  handleSearch,
  filterData,
}) => {
  const theme = useTheme();
  const { width } = useWindowDimensions();
  const { colors }: { colors: IPalette } = theme;
  const styles = React.useMemo(() => createStyles(theme), [theme]);
  const [searchMode, setSearchMode] = React.useState<boolean>(false);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={open}
      onRequestClose={() => {
        setOpen(false);
      }}
    >
      <HeaderModalTokopedia
        open={open}
        setOpen={setOpen}
        searchMode={searchMode}
        setSearchMode={setSearchMode}
        onChanged={handleSearch}
      />
      <View style={styles.modalContainer}>
        {filterData && filterData.length > 0 ? (
          <FilterTokopedia filterData={filterData} />
        ) : (
          <>
            <Image
              style={{
                width: width / 2,
                height: width / 2,
              }}
              source={{
                uri: "https://www.iconlogovector.com/uploads/image/2022/01/tokopedia.png",
              }}
            />
            <View>
              <Button
                color={colors.tokopedia}
                mode={"contained"}
                onPress={() => {
                  setSearchMode(true);
                }}
              >
                <P color={"white"}>Search Product</P>
              </Button>
            </View>
          </>
        )}
      </View>
    </Modal>
  );
};

export default ModalTokopedia;
