import * as React from "react";
import { useTheme } from "@react-navigation/native";
import { View, useWindowDimensions, Image } from "react-native";
import { FAB, Divider } from "react-native-paper";
import { IPalette } from "@theme/themes";
import createStyles from "../styles";
import { IFilterTokopedia, IFilterTokopediaMap } from "../interface";
import BottomDrawer from "react-native-bottom-drawer-view";
import { P } from "@shared-components/Typography";

interface FilterTokopediaProps {
  filterData: IFilterTokopedia;
}

const FilterTokopedia: React.FC<FilterTokopediaProps> = ({ filterData }) => {
  const theme = useTheme();
  const { height } = useWindowDimensions();
  const { colors }: { colors: IPalette } = theme;
  const styles = React.useMemo(() => createStyles(theme), [theme]);
  const [open, setOpen] = React.useState(false);
  // const [selected, setSelected] = React.useState("");

  return (
    <>
      {open ? (
        <BottomDrawer
          borderTopLeftRadius={15}
          borderTopRightRadius={15}
          containerHeight={height / 2}
          offset={height / 4}
        >
          <View
            style={{
              padding: 10,
              backgroundColor: colors.background,
            }}
          >
            <P fontStyle={"bold"} size={14}>
              Filter
            </P>
          </View>
          <View
            style={[
              {
                backgroundColor: colors.background,
                height: "100%",
                paddingHorizontal: 10,
              },
            ]}
          >
            <Divider />
            <View
              style={{
                paddingVertical: 10,
              }}
            >
              <P fontStyle={"bold"}>Choose Category</P>
              {/* <P>
                {
                  JSON.stringify(filterData)
                }
              </P> */}
              {filterData.map((item: IFilterTokopediaMap, i: number) => {
                return (
                  <View key={`filter-1-${i}`}>
                    <Image
                      source={{ uri: item.icon }}
                      style={{
                        height: 50,
                        width: 50,
                      }}
                    />
                  </View>
                );
              })}
            </View>
          </View>
        </BottomDrawer>
      ) : null}
      <FAB
        icon={!open ? "filter" : "close"}
        style={styles.fabContainer}
        color={colors.white}
        onPress={() => {
          setOpen((prev) => {
            return !prev;
          });
        }}
      />
    </>
  );
};

export default FilterTokopedia;
