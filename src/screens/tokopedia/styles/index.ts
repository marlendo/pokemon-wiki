import { ExtendedTheme } from "@react-navigation/native";
import { IPalette } from "@theme/themes";
import { ViewStyle, StyleSheet } from "react-native";

interface Style {
  container: ViewStyle;
  modalContainer: ViewStyle;
  fabContainer: ViewStyle;
}

export default (theme: ExtendedTheme) => {
  const { colors }: { colors: IPalette } = theme;
  return StyleSheet.create<Style>({
    container: {
      flex: 1,
      backgroundColor: colors.background,
      alignItems: "center",
      justifyContent: "center",
    },
    modalContainer: {
      flex: 1,
      backgroundColor: colors.background,
      alignItems: "center",
      justifyContent: "center",
    },
    fabContainer: {
      position: "absolute",
      bottom: 10,
      right: 10,
      backgroundColor: colors.tokopedia,
    },
  });
};
