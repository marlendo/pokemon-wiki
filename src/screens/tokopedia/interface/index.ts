export interface IScrapeQueryFilyer {
  query: string;
}
export interface IFilterTokopediaMap {
  icon: string;
  name: string;
  value: string;
  child: IFilterTokopedia;
  length: number;
  map: (e: any) => void;
}

export interface IFilterTokopedia {
  icon: string;
  name: string;
  value: string;
  child: IFilterTokopedia;
  length: number;
  map: (e: any) => void;
}
[];
