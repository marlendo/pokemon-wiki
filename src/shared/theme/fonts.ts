export interface IFontFamily {
  black: string;
  blackItalic: string;
  bold: string;
  boldItalic: string;
  extraBold: string;
  extraBoldItalic: string;
  extraLight: string;
  extraLightItalic: string;
  italic: string;
  light: string;
  lightItalic: string;
  medium: string;
  mediumItalic: string;
  regular: string;
  semiBold: string;
  semiBoldItalic: string;
  thin: string;
  thinItalic: string;
}

export const montserrat: IFontFamily = {
  black: "Montserrat-Black",
  blackItalic: "Montserrat-BlackItalic",
  bold: "Montserrat-Bold",
  boldItalic: "Montserrat-BoldItalic",
  extraBold: "Montserrat-ExtraBold",
  extraBoldItalic: "Montserrat-ExtraBoldItalic",
  extraLight: "Montserrat-ExtraLight",
  extraLightItalic: "Montserrat-ExtraLightItalic",
  italic: "Montserrat-Italic",
  light: "Montserrat-Light",
  lightItalic: "Montserrat-LightItalic",
  medium: "Montserrat-Medium",
  mediumItalic: "Montserrat-MediumItalic",
  regular: "Montserrat-Regular",
  semiBold: "Montserrat-SemiBold",
  semiBoldItalic: "Montserrat-SemiBoldItalic",
  thin: "Montserrat-Thin",
  thinItalic: "Montserrat-ThinItalic",
};
