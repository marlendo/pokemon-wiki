// ? Screens
export const SCREENS = {
  HOME: "Home",
  SEARCH: "Search",
  NOTIFICATION: "Notification",
  PROFILE: "Profile",
  EXAMPLE: "Example",
  DETAIL: "Detail",
  TOKOPEDIA: "Tokopedia",
};
