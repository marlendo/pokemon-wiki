import React from "react";
import RNText, { IRNTextProps } from "@freakycoder/react-native-custom-text";
import { montserrat } from "@fonts";

interface ITextWrapperProps extends IRNTextProps {
  color?:
    | "primary"
    | "secondary"
    | "background"
    | "white"
    | "black"
    | "button"
    | "shadow"
    | "text"
    | "borderColor"
    | "borderColorDark"
    | "placeholder"
    | "danger"
    | "title"
    | "separator"
    | "highlight"
    | "blackOverlay"
    | "iconWhite"
    | "iconBlack"
    | "dynamicWhite"
    | "dynamicBlack"
    | "dynamicBackground"
    | "transparent"
    | "calpyse"
    | "tokopedia";
  fontStyle?:
    | "black"
    | "blackItalic"
    | "bold"
    | "boldItalic"
    | "extraBold"
    | "extraBoldItalic"
    | "extraLight"
    | "extraLightItalic"
    | "italic"
    | "light"
    | "lightItalic"
    | "medium"
    | "mediumItalic"
    | "regular"
    | "semiBold"
    | "semiBoldItalic"
    | "thin"
    | "thinItalic";
  children?: React.ReactNode;
  size?: number;
  textTransform?: "capitalize" | "uppercase" | "lowercase";
}

export const P: React.FC<ITextWrapperProps> = ({
  fontStyle = "regular",
  color = "black",
  size = 12,
  children,
  textTransform = "capitalize",
  ...rest
}) => {
  return (
    <RNText
      style={{
        color: color,
        fontSize: size,
        fontFamily: montserrat[fontStyle],
        textTransform: textTransform,
      }}
      {...rest}
    >
      {children}
    </RNText>
  );
};
