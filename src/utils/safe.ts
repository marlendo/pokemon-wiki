// example mapSafe(() => response.data.meta.total, 0)
export const mapSafe = (fn: any, defaultVal: any) => {
  try {
    return fn();
  } catch (_e) {
    return defaultVal;
  }
};
